﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;


public class ExperienceController : MonoBehaviour
{
    [Header("Assign These!")]
    [Tooltip("Assign me from the scene!")]
    public SoccerPlayerController soccerPlayerController;

    [Tooltip("Assign me from the scene!")]
    public Transform slideTackleStartLocation;

    [Tooltip("Assign me from the scene!")]
    public Transform slideTackleEndLocation;

    [Tooltip("Assign me from the scene!")]
    public Transform opponentStartLocation;

    [Tooltip("Assign me from the scene!")]
    public Transform soccerBallLocation;
    public GameObject ball;

    [Tooltip("Assign me from the scene!")]
    public AudioSource sceneAudioSource;

    [Header("Sound")]
    public AudioClip startSound;
    public AudioClip ambientTrack;
    public bool mute = false;

    private bool evaluateSoccerPlayerLocation = false;
    private bool tackleStarted = false;
    private float slideTackleDistanceThreshoold = 0.5f;
    private InputDevice rightHand;


    void Start()
    {
        // Set up audio stuff
        sceneAudioSource.loop = true;
        sceneAudioSource.clip = ambientTrack;
        if (!mute) sceneAudioSource.Play();

        var inputDevices = new List<InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Controller, inputDevices);


        if (inputDevices.Count > 1)
        {
            foreach (var device in inputDevices)
            {
                if (device.role == InputDeviceRole.RightHanded)
                {
                    rightHand = device;
                }
            }
        }
        else if (inputDevices.Count == 1)
        {
            rightHand = inputDevices[0];
        }
    }
    void Update()
    {
        if (evaluateSoccerPlayerLocation) evaluateSoccerPlayerLocationLoop();

        if (Input.GetKeyDown("space"))
        {
            Debug.Log(rightHand.manufacturer);
        }

        if (rightHand.TryGetFeatureValue(CommonUsages.triggerButton, out bool triggerValue)
                    && triggerValue && !tackleStarted)
        {
            Debug.Log("trigger");
            tackleStarted = true;
            startTackleExperience();
        }
    }

    [ContextMenu("startTackleExperience")]
    public void startTackleExperience()
    {
        soccerPlayerController.IsRunning = true;
        soccerPlayerController.Target = slideTackleStartLocation.position;
        evaluateSoccerPlayerLocation = true;
        if (!mute) sceneAudioSource.PlayOneShot(startSound);

        ball.transform.position = soccerBallLocation.position;
        ball.transform.rotation = soccerBallLocation.rotation;
        ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
        ball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

    }

    private void evaluateSoccerPlayerLocationLoop()
    {
        if (Vector3.Distance(soccerPlayerController.gameObject.transform.position, slideTackleStartLocation.position) < slideTackleDistanceThreshoold)
        {
            evaluateSoccerPlayerLocation = false;
            soccerPlayerController.StartTackle();
            soccerPlayerController.Target = slideTackleEndLocation.position;
            // Wait unitl tackle is over, then return to start
            // TODO: Come up with a better system than chain invokes lol
            Invoke("returnToStart", 2);
        }
    }

    private void runBackwards()
    {
        soccerPlayerController.RunBackwards();
        soccerPlayerController.Target = slideTackleStartLocation.position;
        Invoke("returnToStart", 1);
    }

    // TODO: Come up with a better system than chain invokes lol (remove)
    private void returnToStart()
    {
        soccerPlayerController.Target = opponentStartLocation.position;
        Invoke("facePlayer", 3);
    }

    // TODO: Come up with a better system than chain invokes lol (remove)
    private void facePlayer()
    {
        soccerPlayerController.IsRunning = false;
        soccerPlayerController.FaceDirection(opponentStartLocation.forward);

        tackleStarted = false;
    }
}
